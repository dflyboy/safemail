const config = require('./config.js');
const tools = require('./tools.js');

const https = require('https');
var fs = require('fs');

const express = require('express');

var multer = require('multer');
var upload = multer({
    storage: multer.memoryStorage()
});

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({
    extended: false
});


const app = express();

function webserver(mailer) {

    global.showWarning = true;

    app.use(express.static(config.web.staticLocation));
    app.set('views', config.web.templateLocation);
    app.set('view engine', 'pug');

    app.get('/', (req, res) => {
        let opt = {
            title: 'Home',
            // showWarning: this.showWarning
        };

        if (req.query.s) {
            opt.sMessage = req.query.s;
        } else if (req.query.e) {
            opt.eMessage = req.query.e;
        } else if (!mailer.isLoggedIn()) {
            return res.redirect('/login');
        }

        res.render('home', opt);
    });

    app.get('/acceptWarning', (req, res) => {
        global.showWarning = false;
        res.end();
    });

    app.get('/login', (req, res, next) => {

        if (mailer.isLoggedIn()) {
            return next('Already logged in.');
        }

        mailer.getAccountList(accs => {
            res.render('login', {
                title: 'Login',
                accounts: accs
            });
        });

    });

    app.get('/fetch', (req, res) => {
        mailer.fetchMail((result, error) => {
            res.send(result);
        })
    });

    app.post('/login', urlencodedParser, (req, res, next) => {
        mailer.login(req.body, err => {
            if (err) {
                return next(err);
            }
            return res.redirect('/?s=' + encodeURIComponent('Logged in'));
        });
    });

    app.get('/import', (req, res) => {
        res.render('import', {
            title: 'Import Key'
        });
    });

    app.post('/import', upload.single('file'), (req, res, next) => {
        if (req.body.paste) {
            mailer.importKey(req.body.paste, req.body.name, (msg, err) => {
                if (err) {
                    return next((err));
                }
                res.redirect('/?s=' + encodeURIComponent(msg));

            });
        } else if (req.file) {
            mailer.importKey(req.file.buffer.toString(), req.body.name, (msg, err) => {
                if (err) {
                    next((err));
                }
                res.redirect('/?s=' + encodeURIComponent(msg));
            });
        } else {
            next(('Paste or upload asc file'));
        }

    });

    app.get('/export', (req, res) => {

        if (req.query.key) {

            let slices = req.query.key.split('-');
            let keyId = parseInt(slices[1]);

            console.log(slices);

            if (slices[0] === 'pub') {

                mailer.getPubKey(keyId, (pub) => {

                    res.render('viewExport', {
                        title: 'Export Key',
                        key: pub
                    });

                });

            } else if (slices[0] === 'priv') {
                mailer.getPrivKey(keyId, (priv) => {

                    res.render('viewExport', {
                        title: 'Export Key',
                        key: priv
                    });

                });
            }

        } else {

            mailer.getPrivKeyList((priv) => {
                mailer.getPubKeyList((pub) => {
                    res.render('exportForm', {
                        title: 'Export Key',
                        privKeys: priv,
                        pubKeys: pub
                    });
                });
            });
        }
    });

    app.post('/export', urlencodedParser, (req, res, next) => {
        if (req.body.key) {

            let slices = req.body.key.split('-');
            let keyId = parseInt(slices[1]);

            console.log(slices);

            if (slices[0] === 'pub') {

                mailer.getPubKey(keyId, (pub) => {

                    res.render('viewExport', {
                        title: 'Export Key',
                        key: pub
                    });

                });

            } else if (slices[0] === 'priv') {
                mailer.getPrivKey(keyId, (priv) => {

                    res.render('viewExport', {
                        title: 'Export Key',
                        key: priv
                    });

                });
            }

        } else {
            next(('no key selected'));
        }

    });

    app.get('/compose', (req, res) => {
        let reply = {
            to: '',
            subject: ''
        };
        if (req.query.to) reply.to = req.query.to;
        if (req.query.subject) reply.subject = req.query.subject;

        mailer.getPubKeyList((pubKeys) => {
            res.render('compose', {
                title: 'Send Mail',
                keys: pubKeys,
                reply: reply
            });
        });
    });

    app.post('/send', urlencodedParser, (req, res, next) => {
        if (!req.body.text || !req.body.destination || !req.body.subject || !req.body.key) return next('Form not complete');

        let keyId = parseInt(req.body.key);
        let mailOpt = {
            keyId: keyId,
            msg: req.body.text,
            to: req.body.destination,
            subject: req.body.subject,
        };
        mailer.sendEncrypted(mailOpt, (s, err) => {
            if (err) {
                next((err));
            } else {
                res.redirect('/?s=' + encodeURIComponent(s));
            }
        });
    });

    app.get('/inbox', (req, res, next) => {

        let num = 10;
        if (req.query.n) num = parseInt(req.query.n);

        res.render('inbox', {
            title: "Inbox",
            wide: true
        });

        // mailer.fetchMail((mails, err) => {

        //     if (err) {
        //         return next(err);
        //         // return res.redirect('/?e=' + encodeURIComponent(err));
        //     }

        //     let num = 10;
        //     if (req.query.n) num = parseInt(req.query.n);

        //     let mailsClipped = mails.reverse().slice(0, num);

        //     res.render('inbox', {
        //         title: "Inbox",
        //         mails: mailsClipped,
        //         wide: true
        //     });

        // });
    });

    app.get('/getMails', (req, res, next) => {
        let nMails = 15;
        if (req.query.n) nMails = parseInt(req.query.n);

        mailer.listMails(nMails)
            .then(mails => {
                res.set('Content-Type', 'application/json');

                res.send(JSON.stringify(mails));

            }, err => {
                res.status(500);
                res.send(err);
                console.log(err);
            });

    });

    app.get('/decrypt', (req, res, next) => {
        if (req.query.seqno) {

            mailer.getPrivKeyList((privKeys) => {
                res.render('decryptForm', {
                    title: 'Decrypt Mail',
                    keys: privKeys,
                    mailNo: req.query.seqno,
                });
            });
        } else {
            return next('no mail given')
        }
    });

    app.post('/viewmail', urlencodedParser, (req, res, next) => {
        if (!req.body.key || !req.body.passphrase || !req.body.seqno) return next('Form not complete');

        let keyId = parseInt(req.body.key);
        let passphrase = req.body.passphrase;
        mailer.decryptMail(keyId, passphrase, req.body.seqno, (plainMail, err) => {
            if (err) {
                return next((err));
            } else {
                let reLink = '/compose?to=' + encodeURIComponent(plainMail.header.from) + '&subject=' + encodeURIComponent('Re: ' + plainMail.header.subject);
                res.render('mail', {
                    title: 'Decrypt Mail',
                    mail: plainMail,
                    replyLink: reLink,
                    wide: true
                });
                if(req.body.delete) mailer.deleteMail(req.body.seqno);

            }
        });
    });

    app.get('/deleteMail', (req, res, next) => {
        if (!req.query.seqno) return next("no mail specified");

        mailer.deleteMail(req.query.seqno).then(() => {
            res.redirect('/?s=' + encodeURIComponent("mail deleted"));
        }, err => {
            return next(err);
        })

    });

    app.get('/generate', (req, res) => {

        res.render('generate', {
            title: 'Generate Keypair',
        });

    });

    // ajax 
    app.post('/genKey', urlencodedParser, (req, res, next) => {
        if (!req.body.keyname || !req.body.passphrase) return next('Form not complete');

        res.type('json');

        mailer.generateKey(req.body.keyname, req.body.passphrase, err => {
            console.log("finished key generation");
            var response = {
                success: true,
                error: ''
            };

            if (err) {
                response.success = false;
                response.error = err;
                console.log("error generating key", err)
            }

            var jsResponse = JSON.stringify(response);

            res.send(jsResponse);

        });
    });

    app.get('/logout', (req, res) => {
        res.sendStatus(200);
        process.exit();
    });

    app.use(this.errorHandler);

    // var privateKey = fs.readFileSync('ssl/key.pem');
    // var certificate = fs.readFileSync('ssl/cert.pem');

    // https.createServer({
    //     key: privateKey,
    //     cert: certificate
    // }, app).listen(config.web.tls_port, () => console.log('listening with ssl on port ' + config.web.tls_port));

    app.listen(config.web.port, () => {
        console.log('listening on port ' + config.web.port);
    });

}

webserver.prototype.errorHandler = function (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.redirect('/?e=' + encodeURIComponent(err));
}

module.exports = webserver;