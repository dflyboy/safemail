const { exec } = require('child_process');

exports.validate = function() {
    var valid = true;
    for (var i=0; i<arguments.length; i++) {
        if( typeof arguments[i] === 'undefined' || arguments[i] === null ) {
            valid = false;
        } else {
            if( typeof arguments[i] === 'string' ) {
                if( arguments[i] === '' ) {
                    valid = false;
                }
            }
        }
    }
    return valid;
}

exports.openBrowser = function(url) {
    switch (process.platform) {
        case 'linux':
            exec('xdg-open "' + url + '"');
            break;
        case 'win32':
            exec('start ' + url);
            break;
        case 'darwin':
            exec('open "' + url + '"');
            break;
    }
}

exports.timeDifference = function timeDifference(current, previous) {

    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago';
    } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + ' days ago';
    } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + ' months ago';
    } else {
        return Math.round(elapsed / msPerYear) + ' years ago';
    }
};