#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const os = require('os');

const config = require('./config.js');

const dbPath = path.dirname(config.db.location);

if(! fs.existsSync( dbPath ) ) {
    fs.mkdirSync( dbPath );
}

if( process.platform === 'win32' ) {
    let homedir = os.homedir();
    fs.linkSync( __dirname + '\\start-windows.cmd', homedir + '\\Desktop\\SafeMail.cmd');
}

process.exit(0);