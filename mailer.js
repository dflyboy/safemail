const crypto = require('crypto');
const fs = require('fs');
const config = require('./config.js');
var tools = require('./tools.js');

var openpgp = require('openpgp');

openpgp.initWorker({
    path: 'openpgp.worker.js'
});
openpgp.config.aead_protect = false;

const nodemailer = require('nodemailer');
var Imap = require('imap');

const Sequelize = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    database: 'safemail',
    storage: config.db.location,
    sync: {
        force: false
    },
    logging: false
});

const Priv = sequelize.define('privKey', {
    name: {
        type: Sequelize.STRING
    },
    key: {
        type: Sequelize.TEXT
    }
});

const Pub = sequelize.define('pubKey', {
    name: {
        type: Sequelize.STRING
    },
    key: {
        type: Sequelize.TEXT
    }
});

const Account = sequelize.define('account', {
    title: Sequelize.STRING,
    username: Sequelize.STRING,
    address: Sequelize.STRING,
    password: Sequelize.STRING,
    smtpHost: Sequelize.STRING,
    smtpPort: Sequelize.INTEGER,
    imapHost: Sequelize.STRING,
    imapPort: Sequelize.INTEGER

});

function mailer() {}

mailer.prototype.connectDb = function (cb) {
    sequelize
        .authenticate()
        .then(() => {
            console.log('DB Connection has been established successfully.');

            sequelize.sync().then(() => {
                console.log('DB synced.');
                return cb();
            });

        })
        .catch(err => {
            return console.error('Unable to connect to the database:', err);
        });
}

mailer.prototype.importKey = function (asc, name, cb) {
    var result = openpgp.key.readArmored(asc);
    // console.log(result);

    if (result.keys.length < 1) {
        cb(null, 'No key found in file');
    }

    result.keys.forEach((key) => {

        if (key.isPrivate()) {
            console.log("Importing private key");

            Priv.create({
                    name: name,
                    key: key.armor()
                })
                .then(() => {
                    cb("Imported", null);
                });
        } else {
            console.log("Importing public key");

            Pub.create({
                    name: name,
                    key: key.armor()
                })
                .then(() => {
                    cb("Imported", null);
                });
        }

    });
}

mailer.prototype.generateKey = function (name, pass, cb) {

    console.log('Generating key %s', name);

    let options = {
        userIds: [{
            name: name
        }], // multiple user IDs
        numBits: 4096, // RSA key size
        passphrase: pass // protects the private key
    };
    openpgp.generateKey(options).then(key => {
            var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
            var pubkey = key.publicKeyArmored; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
            Priv.create({
                    name: name + ' (Private)',
                    key: privkey
                })
                .then(() => {
                    Pub.create({
                            name: name + ' (Public)',
                            key: pubkey
                        })
                        .then(() => {
                            return cb(null);
                        })
                        .catch(err => {
                            return cb(err);
                        });
                })
                .catch(err => {
                    return cb(err);
                });
        })
        .catch(err => {
            return cb(err);
        });
}

mailer.prototype.getPubKeyList = function (cb) {
    Pub.findAll({
        attributes: ['id', 'name']
    }).then(pubKeys => {
        cb(pubKeys);
    });
}

mailer.prototype.getPrivKeyList = function (cb) {
    Priv.findAll({
        attributes: ['id', 'name']
    }).then(privKeys => {
        cb(privKeys);
    });
}

mailer.prototype.getPubKey = function (id, cb) {
    Pub.findById(id).then(key => {
        cb(key);
    })
}

mailer.prototype.getPrivKey = function (id, cb) {
    Priv.findById(id).then(key => {
        cb(key);
    })
}

mailer.prototype.getAccountList = function (cb) {
    Account.findAll().then(accs => {
        return cb(accs);
    });
}

mailer.prototype.login = function (opt, cb) {
    if (opt.existingAcc) {
        console.log('loading exising account');

        Account.findById(opt.existingAcc).then(account => {
            this.loginInfo = {
                address: account.address,
                user: account.username,
                pass: account.password,
                smtp: {
                    hostname: account.smtpHost,
                    port: account.smtpPort
                },
                imap: {
                    hostname: account.imapHost,
                    port: account.imapPort
                }
            };
            return (this.checkLogin(cb));
        }).catch(err => {
            return cb(err);
        });

    } else {

        this.loginInfo = {
            address: opt.address,
            user: opt.username,
            pass: opt.password,
            smtp: {
                hostname: opt.smtpHost,
                port: opt.smtpPort
            },
            imap: {
                hostname: opt.imapHost,
                port: opt.imapPort
            }
        };

        if (opt.save) {
            Account.create(opt).then(() => {
                return (this.checkLogin(cb));
            }).catch(err => {
                if (err) return cb(err);
            });
        } else {
            return (this.checkLogin(cb));
        }

        return this.setupTransport();
    }
};

mailer.prototype.checkLogin = function (cb) {
    let transport = this.setupTransport();
    if (transport) {
        transport.verify((error, success) => {
            if (error) {
                this.loginInfo = null;
                cb(error);
            } else {
                cb(null);
            }
        })
    } else {
        cb('connection error');
    }
}

mailer.prototype.isLoggedIn = function () {
    return (this.loginInfo) ? true : false;
};

mailer.prototype.deleteMail = function(seqno) {
    return new Promise((resolve, reject) => {

        if (!this.loginInfo) {
            return reject("not logged in");
        }

        var imap = new Imap({
            user: this.loginInfo.user,
            password: this.loginInfo.pass,
            host: this.loginInfo.imap.hostname,
            port: this.loginInfo.imap.port,
            tls: true,
        });

        console.log("Deleting mail "+seqno);

        imap.once('ready', function () {
            imap.openBox('INBOX', false, function (err, box) {
                if (err) return reject(err);
                imap.seq.addFlags(seqno, "\\Deleted", err => {
                    if(err) return reject(err);
                    return resolve();
                })
            });
        });

        imap.once('error', function (err) {
            console.log(err);
        });

        imap.connect();
    });
}

mailer.prototype.fetchMail = function (count) {
    return new Promise((resolve, reject) => {

        if (!this.loginInfo) {
            return reject("not logged in");
        }

        var imap = new Imap({
            user: this.loginInfo.user,
            password: this.loginInfo.pass,
            host: this.loginInfo.imap.hostname,
            port: this.loginInfo.imap.port,
            tls: true,
        });

        var mailList = [];

        imap.once('ready', function () {
            imap.openBox('INBOX', true, function (err, box) {
                if (err) return reject(err);
                var f = imap.seq.fetch(box.messages.total - count + ':' + box.messages.total, {
                    bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
                    struct: true
                });
                f.on('message', function (msg, seqno) {
                    var message = {
                        seqno: seqno
                    }

                    msg.on('body', function (stream, info) {
                        var buffer = '';
                        stream.on('data', function (chunk) {
                            buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function () {
                            message.header = Imap.parseHeader(buffer);
                        });
                    });
                    msg.once('attributes', function (attrs) {
                        message.attrs = attrs;
                    });
                    msg.once('end', function () {
                        mailList.push(message);
                    });
                });
                f.once('error', function (err) {
                    reject('Fetch error: ' + err);
                });
                f.once('end', function () {
                    console.log('Done fetching all messages!');
                    imap.end();
                });
            });
        });

        imap.once('error', function (err) {
            console.log(err);
        });

        imap.once('end', function () {
            console.log('Connection ended');
            resolve(mailList);
        });

        imap.connect();
    });
};

mailer.prototype.listMails = function (count) {
    return new Promise((resolve, reject) => {
        var mailList = [];
        this.fetchMail(count)
            .then(mails => {

                mails.forEach((e, index, array) => {
                    var mail = {
                        seqno: e.seqno,
                        uid: e.attrs.uid,
                        from: e.header.from[0],
                        to: e.header.to[0],
                        subject: e.header.subject[0],
                    };
                    mail.date = tools.timeDifference(Date.now(), Date.parse(e.attrs.date));
                    // console.log(mail);
                    mailList.push(mail);
                });

                mailList.reverse();
                resolve(mailList);

            }, err => {
                return reject(err);
            })
    });
}

mailer.prototype.getFullMail = function (mailNo) {
    return new Promise((resolve, reject) => {

        if (!this.loginInfo) {
            return reject("not logged in");
        }

        var imap = new Imap({
            user: this.loginInfo.user,
            password: this.loginInfo.pass,
            host: this.loginInfo.imap.hostname,
            port: this.loginInfo.imap.port,
            tls: true,
        });

        var mailList = [];

        imap.once('ready', function () {
            imap.openBox('INBOX', true, function (err, box) {
                if (err) return reject(err);
                var f = imap.seq.fetch(mailNo, {
                    bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT'],
                    struct: true
                });
                f.on('message', function (msg, seqno) {
                    // console.log('Message #%d', seqno);
                    var message = {
                        seqno: seqno
                    }

                    msg.on('body', function (stream, info) {

                        var buffer = '',
                            count = 0;

                        stream.on('data', function (chunk) {
                            count += chunk.length;
                            buffer += chunk.toString('utf8');
                            // if (info.which === 'TEXT')
                        });

                        stream.once('end', function () {
                            if (info.which !== 'TEXT')
                                message.header = Imap.parseHeader(buffer);
                            else
                                message.text = buffer;
                        });

                    });
                    msg.once('attributes', function (attrs) {
                        message.attrs = attrs;
                    });
                    msg.once('end', function () {
                        mailList.push(message);
                    });
                });
                f.once('error', function (err) {
                    reject('Fetch error: ' + err);
                });
                f.once('end', function () {
                    console.log('Done fetching message '+mailNo);
                    imap.end();
                });
            });
        });

        imap.once('error', function (err) {
            console.log(err);
        });

        imap.once('end', function () {
            console.log('Connection ended');
            resolve(mailList[0]);
        });

        imap.connect();
    });
};

mailer.prototype.decrypt = function (keyId, passphrase, encrypted, cb) {
    Priv.findById(keyId).then(akey => {
        // console.log(akey);
        if (akey) {

            var privKeyObj = openpgp.key.readArmored(akey.key).keys[0];
            privKeyObj.decrypt(passphrase).then(() => {

                let options = {
                    message: openpgp.message.readArmored(encrypted), // parse armored message
                    privateKeys: [privKeyObj] // for decryption
                };

                openpgp.decrypt(options).then(plain => {
                        return cb(plain.data, null);
                    })
                    .catch(err => {
                        return cb(null, err);
                    });

            }).catch(err => {
                return cb(null, err);
            })
        }
    })
}

mailer.prototype.decryptMail = function (keyId, passphrase, mailNo, cb) {

    this.getFullMail(mailNo)
    .then(mail => {

        this.decrypt(keyId, passphrase, mail.text, (plain, error) => {
            if (error) {
                return cb(null, error);
            }
            mail.plain = plain;
            return cb(mail, null);
        });

    }, err => {
        return cb(null, err);
    });
}

mailer.prototype.setupTransport = function () {
    if (this.loginInfo) {
        return nodemailer.createTransport({
            host: this.loginInfo.smtp.hostname,
            port: this.loginInfo.smtp.port,
            secure: true, // true for 465, false for other ports
            ignoreTLS: false,
            auth: {
                user: this.loginInfo.user,
                pass: this.loginInfo.pass
            }
        });
    } else {
        return null;
    }
};

mailer.prototype.sendMail = function (message, cb) {

    let transporter = this.setupTransport();

    if (!transporter) {
        return cb("Not logged in");
    }

    let mailOptions = {
        from: this.loginInfo.address, // sender address
        to: message.to, // list of receivers
        subject: message.subject, // Subject line
        text: message.text, // plain text body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            return cb(error);
        }
        console.log('Message sent: %s', info.messageId);
        cb(null);
    });
};

mailer.prototype.encrypt = function (keyId, message, cb) {
    Pub.findById(keyId).then(akey => {
        // console.log(akey);
        if (akey) {

            let result = openpgp.key.readArmored(akey.key);
            let key = result.keys;

            let options = {
                data: message,
                publicKeys: key
            }

            openpgp.encrypt(options).then(encrypted => {
                    cb(encrypted.data, null);
                })
                .catch(err => {
                    cb(null, err);
                });
        }
    })
}

mailer.prototype.sendEncrypted = function (opt, cb) {


    this.encrypt(opt.keyId, opt.msg, (message, error) => {
        if (error) {
            return cb(null, error);
        }
        let mail = {
            to: opt.to,
            subject: opt.subject,
            text: message,
        };
        this.sendMail(mail, error => {
            if (error) {
                return cb(null, error);
            }
            return cb("Message sent!", null);
        });
    });
}

module.exports = mailer;