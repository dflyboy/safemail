const settings = { 
    web: {
        tls_port: 5555,
        port: 4444,
        templateLocation: __dirname + '/views',
        staticLocation: __dirname + '/static'
    },
    db: {
        location: __dirname + '/data/safemail.db'
    }
};

module.exports = settings;