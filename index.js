#!/usr/bin/env node

var Webserver = require('./web.js');
var config = require('./config.js');
var tools = require('./tools.js');


var Mailer = require('./mailer.js');
var mailer = new Mailer();


mailer.connectDb(() => {
    tools.openBrowser('http://localhost:' + config.web.port + '/login');
});

var web = new Webserver(mailer);