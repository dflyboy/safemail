const buttonHtml = "<button class='ui compact primary icon button'><i class='lock open icon'></i></button>";
const delbuttonHtml = "<button class='ui compact negative icon button'><i class='trash alternate icon'></i></button>";

function loadMails(num) {
    $('#content').dimmer('show');

    $('#mailTable').html("");

    $.getJSON("/getMails?n=" + num)
        .done(function (data) {
            $.each(data, function (key, val) {
                var row = $("<tr><td>" + val.from + "</td><td>" + val.date + "</td><td>" + val.subject + "</td></tr>");
                var buttoncol = $("<td>");
                buttoncol.append($("<a>").attr("href", "/deleteMail?seqno=" + val.seqno).html(delbuttonHtml));
                buttoncol.append($("<a>").attr("href", "/decrypt?seqno=" + val.seqno).html(buttonHtml));
                row.append(buttoncol);

                row.appendTo("#mailTable");
            });

            $('#content').dimmer('hide');
        })
        .fail(function (err) {
            console.log(err);
            // window.location.href = '/?e=' + encodeURIComponent(err);
        });
}

$(document)
    .ready(function () {
        loadMails(10);
    });

$('form').submit(function (e) {
    e.preventDefault();

    loadMails( $('#nMails').val() );
});