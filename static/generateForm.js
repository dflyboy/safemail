$(document)
    .ready(function () {

        const txt = [
            'Negotiating peace in the Middle East',
            'Melting the Arctic ice caps',
            'Reviving Jesus Christ',
            '(The Original G!)',
            'Reuniting Korea',
            'Invading France',
            'Hanging Jesus on the cross',
            'Killing all the polar bears'
        ];

        var subNum = 0;
        var subTime = 4000;

        $('#gForm').form();

        $('#gForm').submit(function (e) {
            e.preventDefault();

            //        console.log($('#contact').serialize());


            $.post('/genKey', $('#gForm').serialize(), function (d, e) {
                console.log(d);
                if (e) {
                    err(e);
                } else {
                    var s = JSON.parse(d);
                    if (s.success) {
                        suc('Keypair successfully generated!');
                    } else {
                        err(s.error);
                    }
                }

            });

            $('.page.dimmer:first')
                .dimmer('set active');
            switchText();
        });

        function switchText() {

            $('#loadtext').fadeOut(500, function () {
                $('#loadtext').text(txt[subNum]).fadeIn(500, function () {
                    setTimeout(switchText(), 100);
                }).delay(subTime);
                if (subNum >= txt.length) {
                    subNum = 0;
                } else {
                    subNum = subNum + 1;
                }
            });
        }


        function err(msg) {
            window.location.href = '/?e=' + encodeURIComponent(msg);
        }

        function suc(msg) {
            window.location.href = '/?s=' + encodeURIComponent(msg);
        }

    });