$(document)
    .ready(function () {
        $('.ui.modal')
            .modal({
                closable: false,
                onApprove: function () {
                    $.get('/acceptWarning');
                }
            })
            .modal('show');
    });